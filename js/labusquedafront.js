//Jquery para la seleccion de pasos
$(function() {
    $(document).on('click', 'div.steps a.steps-mecanica', function(event) {
        var $this = $(this);
        $('html, body').stop().animate({
            scrollTop: $($this.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
        $('section.mecanica-section a, section.mecanica-section p').removeClass('active'); 
        $this.addClass('active');
        $($this.attr('href')).addClass('active');
    });
});

$(function() {
    $(document).on('click', 'div.steps a.steps-calendario', function(event) {
        var $this = $(this);
        $('html, body').stop().animate({
            scrollTop: $($this.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
        $('section.calendario-section a, section.calendario-section div').removeClass('active'); 
        $this.addClass('active');
        $($this.attr('href')).addClass('active');
    });
});

$(function() {
    $(document).on('click', 'div.steps a.steps-ganadores', function(event) {
        var $this = $(this);
        $('html, body').stop().animate({
            scrollTop: $($this.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
        $('section.ganadores-section a, section.ganadores-section div').removeClass('active'); 
        $this.addClass('active');
        $($this.attr('href')).addClass('active');
    });
});